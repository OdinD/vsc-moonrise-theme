# MoonRise Theme
VS Code syntax highlighting theme based on [Eclipse MoonRise UI][1] theme, which later became the default dark theme in the Eclipse Java IDE. Currently only supports Java syntax, but TypeScript is also planned. Uses [One Dark Pro][2] theme as a fallback for other languages and the editor theme.

## Installing
You can install this extension from the following places:
- [Visual Studio Marketplace][8]
- [Open VSX Registry][9]
- [VSIX File][10]

## Requirements
- This theme relies upon the new [semantic highlighting][3] feature introduced in VS Code 1.43.

- For Java syntax highlighting, you need at least version `0.65.0` of the [Language Support for Java(TM)][4] extension. After installation, you also need to make sure that the setting `java.semanticHighlighting.enabled` is set to true in your VS Code settings.

## Notes
- The new semantic highlighting feature in VS Code might take a few seconds to kick in when you first open a project, since it relies upon a language server that provides semantic tokens. This means that the full syntax highlighting theme seen in the screenshot below will be inactive until the [Language Support for Java(TM)][4] extension has successfully activated its language server.

- If you find missing syntax colors, or ones that seem a bit off, please [submit an issue][5]. I have only added necessary syntax colors for my own code, and it is possible that certain areas of the language have been overlooked. Be sure to include an example of code where you expect a different result from the syntax highlighting. 

## Screenshots

### MoonRise semantic Java highlighting:
![semantic-java-highlighting][6]

### Without semantic highlighting:
![default-java-highlighting][7]

[1]: https://github.com/guari/eclipse-ui-theme
[2]: https://github.com/Binaryify/OneDark-Pro
[3]: https://github.com/microsoft/vscode/wiki/Semantic-Highlighting-Overview
[4]: https://github.com/redhat-developer/vscode-java
[5]: https://gitlab.com/OdinD/vsc-moonrise-theme/-/issues/new

[6]: https://gitlab.com/OdinD/vsc-moonrise-theme/-/raw/master/images/semantic-java-highlighting.png "Semantic Java Highlighting"
[7]: https://gitlab.com/OdinD/vsc-moonrise-theme/-/raw/master/images/default-java-highlighting.png "Default Java Highlighting"
[8]: https://marketplace.visualstudio.com/items?itemName=odind.vsc-moonrise-theme
[9]: https://open-vsx.org/extension/odind/vsc-moonrise-theme
[10]: https://gitlab.com/OdinD/vsc-moonrise-theme/-/jobs/artifacts/master/raw/vsc-moonrise-theme.vsix?job=package_vsix
