# Change Log

## [0.2.2] - 2020-07-25
- Added missing syntax color for Java binary and octal numbers
- Added missing syntax color for Javadoc keywords
- Added more TextMate scopes as fallback when semantic tokens are unavailable

## [0.2.1] - 2020-07-24
- Fixed broken link in README.md

##  [0.2.0] - 2020-07-23
- Requires at least version 0.65.0 of the Language Support for Java(TM) extension for correct highlighting
- Complete overhaul of the syntax coloring to more closely match the ones from the original Eclipse theme
- Added more specific syntax colors for Java clases, interfaces, enums etc.
- Added more specific syntax colors for method and local variable declarations.
- Added more specific syntax colors for annotation members
- Added more specific syntax colors for parameters
- Added more TextMate scopes as fallback when semantic tokens are unavailable

## [0.1.5] - 2020-06-22
- Requires at least version 0.63.0 of the Language Support for Java(TM) extension for correct highlighting
- Slightly changed some syntax colors to more closely match the ones from the original Eclipse theme
- Added separate syntax color for Java annotations

## [0.1.4] - 2020-06-03
- Fixed syntax color for Java class keyword

## [0.1.3] - 2020-05-21
- Added missing syntax color for Java primitive array
- Added missing syntax color for Java bitwise operators

## [0.1.2] - 2020-05-14
- Added missing syntax color for Java throw keyword
- Added missing syntax color for Java instanceof keyword
- Added missing syntax color for Java super keyword

## [0.1.1] - 2020-05-02
- Added missing syntax color for Java class keyword
- Added missing syntax color for Java generics wildcard

## [0.1.0] - 2020-05-01
- Initial release
